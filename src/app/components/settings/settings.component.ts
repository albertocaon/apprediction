import { Component, OnInit } from '@angular/core';
import { SelectionService } from '../../services/selection.service';
import { Obj, backs, suits, ranks } from '../../interfaces/card';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

	backs: Obj[] = backs;
	suits: Obj[] = suits;
	ranks: Obj[] = ranks;

	constructor(public _sel: SelectionService) { }

	ngOnInit(): void {
	}

  	selectBack(back: Obj) {
		this._sel.selectBack(back);
	}

	selectSuit(suit: Obj) {
		this._sel.selectSuit(suit);
	}

	selectRank(rank: Obj) {
		this._sel.selectRank(rank);
	}

}
