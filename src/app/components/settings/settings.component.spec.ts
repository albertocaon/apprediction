import { ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { SelectionService } from '../../services/selection.service';
import { SettingsComponent } from './settings.component';
import { Obj } from '../../interfaces/card';

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;
  let _sel: SelectionService;
  let injector: TestBed;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsComponent ],
      providers: [ SelectionService ]
    })
    .compileComponents();

     injector = getTestBed();
    _sel = injector.get(SelectionService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select back', () => {
    const blueBack = {
      id: 1,
      name: 'blue'
    };
    const redBack = {
      id: 0,
      name: 'red'
    };
    component.selectBack(blueBack);
    expect(_sel.selected.back).toEqual(blueBack);
    component.selectBack(redBack);
    expect(_sel.selected.back).toEqual(redBack);
  });

  it('should select suit', () => {
    const hearts: Obj = {
      id: 0,
      name: 'hearts'
    };
    const clubs: Obj = {
      id: 3,
      name: 'clubs'
    };
    component.selectSuit(hearts);
    expect(_sel.selected.suit).toEqual(hearts);
    component.selectSuit(clubs);
    expect(_sel.selected.suit).toEqual(clubs);
  });

  it('should select rank', () => {
    const jack: Obj = {
      id: 10,
      name: 'J'
    };
    const king: Obj = {
      id: 12,
      name: 'K'
    };
    component.selectRank(jack);
    expect(_sel.selected.rank).toEqual(jack);
    component.selectRank(king);
    expect(_sel.selected.rank).toEqual(king);
  });
});
