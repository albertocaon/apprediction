import { Component, OnInit } from '@angular/core';
import { SelectionService } from '../../services/selection.service';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
	selector: 'app-game',
	templateUrl: './game.component.html',
	styleUrls: ['./game.component.scss'],
	animations: [
	    trigger('status', [
	      state('visble', style({
	      	opacity: 1
	      })),
	      state('hidden', style({
	      	opacity: 0
	      })),
	      transition('visible => hidden', [
	        animate('3s')
	      ]),
	      transition('hidden => visible', [
	        animate('3s')
	      ]),
	    ]),
	],
})
export class GameComponent implements OnInit {

	revealed: boolean = false;

	constructor(public _sel: SelectionService) { }

	ngOnInit(): void {
	}

	changeStatus() {
		this.revealed = !this.revealed;
	}
}
