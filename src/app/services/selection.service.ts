import { Injectable } from '@angular/core';
import { Card, Obj, blackjack } from '../interfaces/card';

@Injectable({
  providedIn: 'root'
})
export class SelectionService {

	selected: Card = blackjack;

	constructor() { }

	select(card: Card) {
		this.selected = card;
	}

	selectBack(back: Obj) {
		this.selected.back = back;
	}

	selectSuit(suit: Obj) {
		this.selected.suit = suit;
	}

	selectRank(rank: Obj) {
		this.selected.rank = rank;
	}
}
