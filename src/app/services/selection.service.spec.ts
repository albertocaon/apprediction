import { TestBed } from '@angular/core/testing';
import { blackjack, Card, Obj } from '../interfaces/card';
import { SelectionService } from './selection.service';

describe('SelectionService', () => {
  let service: SelectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SelectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have blackjack selected by default', () => {
    expect(service.selected).toEqual(blackjack);
  });

  it('should select Ace of Hearts', () => {
  	const blueAceHearts: Card = {
		back: 
		{
			id: 1,
			name: 'blue'
		},
		suit: 
		{
			id: 0,
			name: 'hearts'
		},
		rank:
		{
			id: 0,
			name: 'A'
		}
	};

  	service.select(blueAceHearts);
    expect(service.selected).toEqual(blueAceHearts);
  });

  it('should select back', () => {
    const blueBack = {
      id: 1,
      name: 'blue'
    };
    const redBack = {
      id: 0,
      name: 'red'
    };
    service.selectBack(blueBack);
    expect(service.selected.back).toEqual(blueBack);
    service.selectBack(redBack);
    expect(service.selected.back).toEqual(redBack);
  });

  it('should select suit', () => {
    const hearts: Obj = {
      id: 0,
      name: 'hearts'
    };
    const clubs: Obj = {
      id: 3,
      name: 'clubs'
    };
    service.selectSuit(hearts);
    expect(service.selected.suit).toEqual(hearts);
    service.selectSuit(clubs);
    expect(service.selected.suit).toEqual(clubs);
  });

  it('should select rank', () => {
    const jack: Obj = {
      id: 10,
      name: 'J'
    };
    const king: Obj = {
      id: 12,
      name: 'K'
    };
    service.selectRank(jack);
    expect(service.selected.rank).toEqual(jack);
    service.selectRank(king);
    expect(service.selected.rank).toEqual(king);
  });
});
