export interface Card {
	"back": Obj;
	"suit": Obj;
	"rank": Obj;
}

export interface Obj {
	"id": number;
	"name": string;
}

export const blackjack: Card = {
	back: 
	{
		id: 0,
		name: 'red'
	},
	suit: 
	{
		id: 2,
		name: 'spades'
	},
	rank:
	{
		id: 10,
		name: 'J'
	}
}

export const backs: Obj[] = [
	{
		id: 0,
		name: 'red'
	},
	{
		id: 1,
		name: 'blue'
	}
];

export const suits: Obj[] = [
	{
		id: 0,
		name: 'hearts'
	},
	{
		id: 1,
		name: 'diamonds'
	},
	{
		id: 2,
		name: 'spades'
	},
	{
		id: 3,
		name: 'clubs'
	}
];

export const ranks: Obj[] = [
	{
		id: 0,
		name: 'A'
	},
	{
		id: 1,
		name: '2'
	},
	{
		id: 2,
		name: '3'
	},
	{
		id: 3,
		name: '4'
	},
	{
		id: 4,
		name: '5'
	},
	{
		id: 5,
		name: '6'
	},
	{
		id: 6,
		name: '7'
	},
	{
		id: 7,
		name: '8'
	},
	{
		id: 8,
		name: '9'
	},
	{
		id: 9,
		name: '10'
	},
	{
		id: 10,
		name: 'J'
	},
	{
		id: 11,
		name: 'Q'
	},
	{
		id: 12,
		name: 'K'
	}
];
