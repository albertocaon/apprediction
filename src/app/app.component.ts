import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PwaService } from './services/pwa.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
	constructor(public router: Router, public _pwa: PwaService) {}

	installPwa(): void {
    	this._pwa.promptEvent.prompt();
  	}
}
