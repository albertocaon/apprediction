import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SettingsComponent } from './components/settings/settings.component';
import { GameComponent } from './components/game/game.component';

const routes: Routes = [
	{
	    path: '',
	    redirectTo: 'game',
	    pathMatch: 'full'
	},
	{
	    path: 'explanation',
	    component: HomeComponent
	},
	{
	    path: 'settings',
	    component: SettingsComponent
	},
	{
	    path: 'game',
	    component: GameComponent
	},
	// All your other routes should come first    
	{ 
	    path: '404', 
	    redirectTo: 'game',
	},
	{ 
	    path: '**', 
	    redirectTo: 'game',
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
